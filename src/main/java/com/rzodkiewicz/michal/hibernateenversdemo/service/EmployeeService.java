package com.rzodkiewicz.michal.hibernateenversdemo.service;

import com.rzodkiewicz.michal.hibernateenversdemo.model.Employee;
import com.rzodkiewicz.michal.hibernateenversdemo.respository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.history.Revision;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    public List<Revision<Integer, Employee> >getRevisions(Long id){
        return employeeRepository.findRevisions(id).getContent();
    }
}
