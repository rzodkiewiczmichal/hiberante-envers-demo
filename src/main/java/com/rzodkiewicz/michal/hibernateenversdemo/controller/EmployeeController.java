package com.rzodkiewicz.michal.hibernateenversdemo.controller;

import com.rzodkiewicz.michal.hibernateenversdemo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class EmployeeController{

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService){
        this.employeeService = employeeService;
    }

    @GetMapping("/employees/{id}/revisions")
    public ResponseEntity getRevisions(@PathVariable("id") Long id){
        return ResponseEntity.ok(this.employeeService.getRevisions(id));
    }
}
