package com.rzodkiewicz.michal.hibernateenversdemo.model;

import lombok.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Audited
public class Project extends IdEntity{

    private String name;
    private String description;

    @ManyToMany(mappedBy = "projects")
    @NotAudited
    private List<Employee> employees;

    @OneToOne
    private Timeline timeline;
}
