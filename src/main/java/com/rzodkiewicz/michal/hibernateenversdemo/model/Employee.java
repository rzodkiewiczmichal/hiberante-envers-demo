package com.rzodkiewicz.michal.hibernateenversdemo.model;

import lombok.*;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Audited
public class Employee extends IdEntity{

    private String firstName;
    private String lastName;

    @ManyToMany
    @JoinTable
    @NotAudited
    private List<Project> projects;



}
