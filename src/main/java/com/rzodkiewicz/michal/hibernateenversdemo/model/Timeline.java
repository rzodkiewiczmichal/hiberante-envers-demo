package com.rzodkiewicz.michal.hibernateenversdemo.model;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.time.ZonedDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Audited
public class Timeline extends IdEntity {

    private ZonedDateTime startDate;
    private ZonedDateTime endDate;

    @OneToOne
    private Project project;
}
