package com.rzodkiewicz.michal.hibernateenversdemo.respository;

import com.rzodkiewicz.michal.hibernateenversdemo.model.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long>, RevisionRepository<Employee, Long, Integer> {
}
