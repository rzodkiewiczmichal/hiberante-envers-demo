package com.rzodkiewicz.michal.hibernateenversdemo.respository;

import com.rzodkiewicz.michal.hibernateenversdemo.model.Timeline;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimelineRepository extends CrudRepository<Timeline, Long>, RevisionRepository<Timeline, Long, Integer> {
}
