package com.rzodkiewicz.michal.hibernateenversdemo.respository;

import com.rzodkiewicz.michal.hibernateenversdemo.model.Project;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long>, RevisionRepository<Project, Long, Integer> {

}
