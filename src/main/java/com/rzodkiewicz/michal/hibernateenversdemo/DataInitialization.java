package com.rzodkiewicz.michal.hibernateenversdemo;

import com.rzodkiewicz.michal.hibernateenversdemo.model.Employee;
import com.rzodkiewicz.michal.hibernateenversdemo.model.Project;
import com.rzodkiewicz.michal.hibernateenversdemo.model.Timeline;
import com.rzodkiewicz.michal.hibernateenversdemo.respository.EmployeeRepository;
import com.rzodkiewicz.michal.hibernateenversdemo.respository.ProjectRepository;
import com.rzodkiewicz.michal.hibernateenversdemo.respository.TimelineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.time.ZonedDateTime;
import java.util.Arrays;

@Configuration
public class DataInitialization {

    private final EmployeeRepository employeeRepository;
    private final ProjectRepository projectRepository;
    private final TimelineRepository timelineRepository;

    @Autowired
    public DataInitialization(EmployeeRepository employeeRepository, ProjectRepository projectRepository,
                              TimelineRepository timelineRepository) {
        this.employeeRepository = employeeRepository;
        this.projectRepository = projectRepository;
        this.timelineRepository = timelineRepository;
    }

    private void initData(){
        Project project1 = new Project("Test project", "Project 1", null, null);
        Project project2 = new Project("Test project", "Project 2", null, null);
        projectRepository.save(project1);
        projectRepository.save(project2);
        Timeline timeline1 = new Timeline(ZonedDateTime.now(), ZonedDateTime.now(), project1);
        Timeline timeline2 = new Timeline(ZonedDateTime.now(), ZonedDateTime.now(), project2);
        timelineRepository.save(timeline1);
        timelineRepository.save(timeline2);
        project1.setTimeline(timeline1);
        project2.setTimeline(timeline2);
        projectRepository.save(project1);
        projectRepository.save(project2);
        Employee employee1 = new Employee("John", "Smith", Arrays.asList(project1, project2));
        Employee employee2 = new Employee("Joe", "Connor", Arrays.asList(project1, project2));
        employeeRepository.save(employee1);
        employeeRepository.save(employee2);

        Project project3 = new Project("Test project", "Project 3", null, null);
        Project project4 = new Project("Test project", "Project 4", null, null);
        projectRepository.save(project3);
        projectRepository.save(project4);
        Timeline timeline3 = new Timeline(ZonedDateTime.now(), ZonedDateTime.now(), project1);
        Timeline timeline4 = new Timeline(ZonedDateTime.now(), ZonedDateTime.now(), project2);
        timelineRepository.save(timeline3);
        timelineRepository.save(timeline4);
        project3.setTimeline(timeline3);
        project4.setTimeline(timeline4);
        projectRepository.save(project3);
        projectRepository.save(project4);
        Employee employee3 = new Employee("John", "Lenon", Arrays.asList(project1, project3));
        Employee employee4 = new Employee("Chris", "Cornell", Arrays.asList(project2, project4));
        employeeRepository.save(employee3);
        employeeRepository.save(employee4);
    }

}
